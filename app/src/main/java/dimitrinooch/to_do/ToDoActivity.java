package dimitrinooch.to_do;
an object that belongs to a particular class is called and instances, and the process of creating an object is called instantiation.
        a class defiintion includes constructors, methods, and data fields. the constructors describe how objects of the class can be created; the methods describe what an object of this class can do; the data fields describe the objects attributes

        ALL CONSTRUCTORS HAVE THE SAME NAE AS THE CLASS. CONSTRUCTORS DO NOT HAVE ANY RETURN DATA TYPE, NOT EVEN VOID.

        THE TERM PARAMETER IS OFTEN INTERCHANGEABLE WITH THE TERM ARGUMENT(AS IN FUNCTION ARGUMENT IN MATH), ESPECIALLY WHEN IT REFERS TO THE ACTUAL VALUES PASSED TO A CONSTRUCTOR OR METHOD PRIVATE FIELDS AND METHODS CAN BE REFERRED TO ONLY WITHIN THE SOURCE CODE OF THE CLASS THEY ARE DEFINED IN .
        THE CONCEPT OF "PRIVACY" APPLIES TO THE SOURECODE OF THE CLASS AS A WHOLE AND NOT TO INDIVIDUAL OBJECTS. DIFFERENT OBJECT OF THE SAME CLASSS HAVE FULL ACCESS TO EACH OTHERS FIELDS AND METHODS, AND CAN EVEN MODIFY THE VALUES OF EAH OTHERS FIELDS
        IT IS COMMON PRACTIVE IN OOP(AND A REQUIREMENT ON THE AP EXAM) TO MAKE ALL INSTANCE VARIABLES PRIVATE
        PRIVATE FIELDS AND METHOSD HIDE THE IMPLEMENTATION DETAILS OF A CLASS FROM OTHER CLASSES. ITS CALLED ENCASPULATION
        ACCESSORS NAMES OFTEN STAR WITH A "GET"
        MODIFIERS NAMES OFTEN START WITH A "SET"
        3.2
        STATIC VARIABLES ARE USED TO KEEP TRACK OF A PROPERTY OR QUEANTIT SHARED BY ALL OBJECTS
        A STATIC VARIABLE CAN BE ALSO USED TO DEFINE A PUBLIC SYMBOLIC CONSTANT STATIC METHODS CANNOT ACCESS OR MODIFY ANY INSTANCE VARIABLES AND CANNOT REFER TO THIS BECAUSE THIS IS UNDEIFINED WHEN A STATIC METHOS IS RUNNING

        import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;

public class ToDoActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<ToDoItem> toDoItems = new ArrayList<ToDoItem>();
    public int currentItem;

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}

//    @Override
//    public void onPostSelected(int pos) {
//        currentItem = pos;
//        Fragment newFragment = new ItemFragment();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.replace(R.id.fragment_container, newFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }



