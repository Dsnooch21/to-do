package dimitrinooch.to_do;

public class ToDoItem {
    public String title;
    public String dateAdd;
    public String dateDue;
    public String category;

    public ToDoItem(String title, String dateAdd, String dateDue, String category){
        this.title = title;
        this.dateAdd = dateAdd;
        this.dateDue = dateDue;
        this.category = category;
    }
}
