package dimitrinooch.to_do;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ToDoListFragment extends Fragment {
    public RecyclerView recyclerView;
   // private ActivityCallback activityCallback;
    private ToDoActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
       // activityCallback = (ActivityCallback)activity;
        this.activity = (ToDoActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
      // activityCallback = null;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem i1 = new ToDoItem("I","10/30/15","11/2/15","App Academy");
        ToDoItem i2 = new ToDoItem("HATE","10/30/15","11/2/15","App Academy");
        ToDoItem i3 = new ToDoItem("REDDIT","10/30/15","11/2/15","App Academy");
        ToDoItem i4 = new ToDoItem("HI BLACK JESUS","10/30/15","11/2/15","App Academy");

        activity.toDoItems.add(i1);
        activity.toDoItems.add(i2);
        activity.toDoItems.add(i3);
        activity.toDoItems.add(i4);

        ToDoListAdapter adapter = new ToDoListAdapter(activity.toDoItems);
        recyclerView.setAdapter(adapter);


        return view;
    }


}


