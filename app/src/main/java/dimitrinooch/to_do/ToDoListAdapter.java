package dimitrinooch.to_do;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListHolder>{

        private ArrayList<ToDoItem> toDoItems;
            // private ActivityCallback activityCallback;

        public ToDoListAdapter(ArrayList<ToDoItem> toDoItems){
            this.toDoItems = toDoItems;
            //this.activityCallback = activityCallback;
        }
        @Override
        public ToDoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            return new ToDoListHolder(view);
        }

        @Override
        public void onBindViewHolder(ToDoListHolder Holder, final int position) {
//            Holder.titleText.setOnClickListener[(v)];
//            activityCallback.onPostselected(position);
            Holder.titleText.setText(toDoItems.get(position).title);
        }

        @Override
        public int getItemCount() {
            return toDoItems.size();
        }
    }

